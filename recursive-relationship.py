def sequence(n): #Creates the sequence function that will be used in the main function.
    last_element = 2 #Element variable 
    list_sequence = [] #List variable
    list_sequence.append(last_element) #Appends the first element in the list
    for i in range(n-1):
        element = 5 * last_element + 2
        last_element = element
        list_sequence.append(element)

    return list_sequence # returns the list

def main():
    while True:
        n = int(input("Please enter the value of N: ")) #variable for N, must be an integer.
        if n <= 0: #Any value entered by the user equal to or less than 0 will break the script.
            raise Exception("Please Try Again...")#Displays this message when the exception is raised.

        list_sequence = sequence(n)
        print(list_sequence)
        break #Breaking the loop

main()
#Runs the function