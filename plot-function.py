import matplotlib.pyplot as plt
import numpy as np
#Importing the librarys and shortening

x = np.arange(-2*np.pi, 2*np.pi, 0.1)
#Determining the value of X

y = np.exp(-x) * np.sin(2*x + np.pi/3)
#Determining the value of Y

plt.plot(x,y)
#Plotting the x and y values on a graph
plt.xlabel("X")
plt.ylabel("Y")
#Giving the X and Y value on the graph
plt.title("Y = e-xsin (2x +𝜋3) , - 2𝜋 ≤ x ≤ 2")
#Title of the graph
plt.show()
#Display the graph